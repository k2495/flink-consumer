package com.sfeir.mbodin.flinkconsumer.rationalization;

import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import com.sfeir.mbodin.kafkaproducer.protobuff.EventOperationKind;
import lombok.extern.log4j.Log4j2;
import org.apache.flink.api.common.functions.ReduceFunction;

@Log4j2
public class EventDataReduceFunction implements ReduceFunction<EventData> {

    private final boolean logIsTraceEnabled;

    public EventDataReduceFunction() {
        logIsTraceEnabled = log.isTraceEnabled();
    }

    @Override
    public EventData reduce(EventData value1, EventData value2) {
        if (logIsTraceEnabled) {
            log.trace("{} <> {}", value1.getOperation().name(), value2.getOperation().name());
        }

        switch (value1.getOperation()) {
            case CREATE:
                return createOnTheleft(value1,value2);
            case UPDATE:
                return updateOnTheleft(value1, value2);
            case DELETE:
                return deleteOnTheleft(value1, value2);
            default:
                return value1;
        }
    }

    private EventData createOnTheleft(EventData value1, EventData value2) {
        final EventData reduce;

        switch (value2.getOperation()) {
            case CREATE:
                reduce = EventDataMerger.newBuilderFrom(value1).mergeWith(value2);
                break;
            case UPDATE:
                reduce = EventDataMerger.newBuilderFrom(value1).mergeWith(value2);
                break;
            default:
                reduce = value1;
                break;
        }

        if (logIsTraceEnabled) {
            log.trace(reduce);
        }

        return reduce;
    }

    private EventData updateOnTheleft(EventData value1, EventData value2) {
        EventData reduce = value1;

        if (value2.getOperation() == EventOperationKind.UPDATE) {
            reduce = EventDataMerger.newBuilderFrom(value1).mergeWith(value2);
        }

        if (logIsTraceEnabled) {
            log.trace(reduce);
        }

        return reduce;
    }

    private EventData deleteOnTheleft(EventData value1, EventData value2) {
        EventData reduce = value1;

        if (value2.getOperation() == EventOperationKind.DELETE) {
            reduce = EventDataMerger.newBuilderFrom(value1).mergeWith(value2);
        }

        if (logIsTraceEnabled) {
            log.trace(reduce);
        }

        return reduce;
    }
}
