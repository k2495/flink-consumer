package com.sfeir.mbodin.flinkconsumer.rationalization;

import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import lombok.extern.log4j.Log4j2;
import org.apache.flink.api.java.functions.KeySelector;

@Log4j2
public class NicknameKeySelector implements KeySelector<EventData, String> {

    private final boolean logIsTraceEnabled;

    public NicknameKeySelector() {
        logIsTraceEnabled = log.isTraceEnabled();
    }

    @Override
    public String getKey(EventData value) {
        if (logIsTraceEnabled) {
            log.trace("key -> {}", value.getCard().getNickname());
        }

        return value.getCard().getNickname();
    }
}
