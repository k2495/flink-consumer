package com.sfeir.mbodin.flinkconsumer.rationalization;

import com.sfeir.mbodin.kafkaproducer.protobuff.AddressData;
import com.sfeir.mbodin.kafkaproducer.protobuff.CardData;
import com.sfeir.mbodin.kafkaproducer.protobuff.ContactInformation;
import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import lombok.NonNull;

import java.util.HashMap;

public class EventDataMerger {

    private final EventData leftOperand;
    private final HashMap<String, ContactInformation> emails;
    private final HashMap<String, ContactInformation> phones;
    private final HashMap<String, AddressData> addresses;

    private EventDataMerger(@NonNull final EventData leftOperand) {
        this.leftOperand = leftOperand;

        emails = new HashMap<>();
        phones = new HashMap<>();
        addresses = new HashMap<>();

        this.leftOperand.getCard().getPhonesList().forEach(ci -> {
            if (ci.hasLabel()) {
                phones.put(ci.getLabel(), ci);
            } else {
                phones.put(ci.getValue(), ci);
            }
        });

        this.leftOperand.getCard().getEmailsList().forEach(ci -> {
            if (ci.hasLabel()) {
                emails.put(ci.getLabel(), ci);
            } else {
                emails.put(ci.getValue(), ci);
            }
        });

        this.leftOperand.getCard().getAddressesList().forEach(a -> {
            if (a.hasLabel()) {
                addresses.put(a.getLabel(), a);
            } else {
                addresses.put(a.getPostcode(), a);
            }
        });
    }

    public static EventDataMerger newBuilderFrom(final EventData leftOperand) {
        return new EventDataMerger(leftOperand);
    }

    public EventData mergeWith(final EventData rightOperand) {
        if (rightOperand == null) {
            return leftOperand;
        }

        final CardData cardData = rightOperand.getCard();

        cardData.getEmailsList().forEach(ci -> {
            if (ci.hasLabel()) {
                emails.remove(ci.getLabel());
            }
        });

        cardData.getPhonesList().forEach(ci -> {
            if (ci.hasLabel()) {
                phones.remove(ci.getLabel());
            }
        });

        cardData.getAddressesList().forEach(a -> {
            if (a.hasLabel()) {
                addresses.remove(a.getLabel());
            }
        });

        return EventData.newBuilder()
                .setSource(rightOperand.getSource())
                .setOperation(rightOperand.getOperation())
                .setCard(cardData.toBuilder()
                        .addAllEmails(emails.values())
                        .addAllPhones(phones.values())
                        .addAllAddresses(addresses.values())
                        .build())
                .build();
    }
}
