package com.sfeir.mbodin.flinkconsumer;

import com.sfeir.mbodin.flinkconsumer.output.EventDataKeySerializer;
import com.sfeir.mbodin.flinkconsumer.output.EventDataValueSerializer;
import com.sfeir.mbodin.flinkconsumer.output.OperationKindTopicSelector;
import com.sfeir.mbodin.flinkconsumer.rationalization.EventDataReduceFunction;
import com.sfeir.mbodin.flinkconsumer.rationalization.NicknameKeySelector;
import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import lombok.extern.log4j.Log4j2;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;

import java.util.Objects;

@Log4j2
public class FlinkConsumerApplication {

    public static void main(String[] args) throws Exception {
        final ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(args);

        // Obtain an execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // Configure flink (Kryo) to internally support serialization operations
        env.getConfig().addDefaultKryoSerializer(EventData.class, EventDataKryoSerializer.class);
        // Configure the source
        final KafkaSource<EventData> source = KafkaSource.<EventData>builder()
                .setBootstrapServers(applicationConfiguration.getBootstrapServers())
                .setPartitions(applicationConfiguration.getTopicPartitions())
                .setGroupId(applicationConfiguration.getGroupId())
                .setClientIdPrefix(applicationConfiguration.getClientId())
                .setStartingOffsets(OffsetsInitializer.committedOffsets(OffsetResetStrategy.EARLIEST))
                .setValueOnlyDeserializer(new EventDataDeserializationSchema())
                .build();
        // Create the data stream
        final DataStreamSource<EventData> datastream = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka source");
        // Specify transformation
        final SingleOutputStreamOperator<EventData> transformation = datastream
                .keyBy(new NicknameKeySelector())
                .window(ProcessingTimeSessionWindows.withGap(applicationConfiguration.getWindowGap()))
                .reduce(new EventDataReduceFunction())
                .setParallelism(2)
                .filter((FilterFunction<EventData>) Objects::nonNull);
        // Specify where to put the results
        transformation.sinkTo(KafkaSink.<EventData>builder()
                .setBootstrapServers(applicationConfiguration.getBootstrapServers())
                .setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE)
                .setRecordSerializer(KafkaRecordSerializationSchema.builder()
                        .setTopicSelector(new OperationKindTopicSelector())
                        .setKafkaKeySerializer(EventDataKeySerializer.class)
                        .setKafkaValueSerializer(EventDataValueSerializer.class)
                        .build()
                ).build()
        ).name("Kafka sink").setParallelism(1);

        //Trigger the program execution
        env.execute();
    }
}
