package com.sfeir.mbodin.flinkconsumer;


import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.DefaultArraySerializers;
import com.google.protobuf.InvalidProtocolBufferException;
import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;

public class EventDataKryoSerializer extends Serializer<EventData> {

    private static final DefaultArraySerializers.ByteArraySerializer SERIALIZER =  new DefaultArraySerializers.ByteArraySerializer();

    @Override
    public void write(Kryo kryo, Output output, EventData object) {
        if (object != null) {
            kryo.writeObject(output, object.toByteArray(), SERIALIZER);
        }
    }

    @Override
    public EventData read(Kryo kryo, Input input, Class<EventData> type) {
        final byte[] bytes = kryo.readObjectOrNull(input, byte[].class, SERIALIZER);

        if (bytes != null) {
            try {
                return EventData.parseFrom(bytes);
            } catch (InvalidProtocolBufferException e) {
                throw new KryoException(e);
            }
        } else return null;
    }
}
