package com.sfeir.mbodin.flinkconsumer.output;

import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import org.apache.kafka.common.serialization.Serializer;

public class EventDataKeySerializer implements Serializer<EventData> {
    @Override
    public byte[] serialize(String topic, EventData data) {
        return data.getCard().getNickname().toLowerCase().getBytes();
    }
}
