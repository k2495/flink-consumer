package com.sfeir.mbodin.flinkconsumer.output;

import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import org.apache.flink.connector.kafka.sink.TopicSelector;

public class OperationKindTopicSelector implements TopicSelector<EventData> {
    @Override
    public String apply(EventData eventData) {
        String marker;

        switch (eventData.getOperation()) {
            case CREATE:
            case UPDATE:
            case DELETE:
                marker = eventData.getOperation().name().toLowerCase();
                break;
            default:
                marker = "lost";
                break;
        }
        return "private_" + marker + "-operations";
    }
}
