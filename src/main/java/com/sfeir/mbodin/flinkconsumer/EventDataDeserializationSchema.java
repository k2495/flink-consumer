package com.sfeir.mbodin.flinkconsumer;

import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;

import java.io.IOException;

public class EventDataDeserializationSchema extends AbstractDeserializationSchema<EventData> {
    @Override
    public EventData deserialize(byte[] message) throws IOException {
        if (message != null) {
            return EventData.parseFrom(message);
        }

        return null;
    }
}
