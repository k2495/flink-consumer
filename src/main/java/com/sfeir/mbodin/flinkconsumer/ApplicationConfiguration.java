package com.sfeir.mbodin.flinkconsumer;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.kafka.common.TopicPartition;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

@Log4j2
public class ApplicationConfiguration {
    @Getter
    private String bootstrapServers;
    @Getter
    private String clientId;
    @Getter
    private String groupId;
    @Getter
    private final Set<TopicPartition> topicPartitions;
    @Getter
    private final Time windowGap;

    public ApplicationConfiguration(String... args) {
        final Properties properties = new Properties();

        // Somehow the application.properties may not be loaded properly, probably due to dirty class loader manipulations
        // when used as Jar in a session mode cluster...
        try {
            final URL path = Thread.currentThread().getContextClassLoader().getResource("");

            if (path != null) {
                properties.load(new FileInputStream(path.getPath() + "application.properties"));
            }
        } catch (IOException e) {
            log.warn("application.properties unreachable, now rely on default values and program arguments", e);
        }

        // Initialize configuration from properties
        String topicName = properties.getProperty("topic.name");
        String topicNumPartition = properties.getProperty("topic.num-partition", "1");
        bootstrapServers = properties.getProperty("application.boostrap-servers");
        clientId = properties.getProperty("application.client-id", "SampleKafkaConsumer");
        groupId = properties.getProperty("application.group-id", "flink-consumers");
        String rationalizationWindowGapInS = properties.getProperty("rationalization.window-gap-in-s", "5");

        // Override configuration from program arguments
        for (int i = 0; i < args.length - 1; i++) {
            switch (args[i]) {
                case "--topic-name":
                    topicName = args[++i];
                    break;
                case "--topic-num-partition":
                    topicNumPartition = args[++i];
                    break;
                case "--bootstrap-servers":
                    bootstrapServers = args[++i];
                    break;
                case "--client-id":
                    clientId = args[++i];
                    break;
                case "--group-id":
                    groupId = args[++i];
                    break;
                case "--rational-window-gap-in-s":
                    rationalizationWindowGapInS = args[++i];
                    break;
            }
        }

        if (bootstrapServers == null) {
            throw new IllegalStateException("bootstrap-servers is not defined");
        } else if (topicName == null) {
            throw new IllegalStateException("topic-name is not defined");
        }

        // Once validated, go on with the initialization still to proceed
        topicPartitions = new HashSet<>();
        for (int i = 0; i < Integer.valueOf(topicNumPartition, 10); i++) {
            topicPartitions.add(new TopicPartition(topicName, i));
        }

        windowGap = Time.seconds(Integer.valueOf(rationalizationWindowGapInS,10));
    }
}
