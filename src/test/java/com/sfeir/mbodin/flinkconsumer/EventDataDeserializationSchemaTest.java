package com.sfeir.mbodin.flinkconsumer;

import com.sfeir.mbodin.kafkaproducer.protobuff.CardData;
import com.sfeir.mbodin.kafkaproducer.protobuff.ContactInformation;
import com.sfeir.mbodin.kafkaproducer.protobuff.EventData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class EventDataDeserializationSchemaTest {
    @Test
    void deserialize() throws IOException {
        final EventDataDeserializationSchema deserializationSchema = new EventDataDeserializationSchema();

        final EventData dataIn = EventData.newBuilder()
                .setSource(UUID.randomUUID().toString())
                .setCard(CardData.newBuilder()
                        .setNickname("jdoe")
                        .addPhones(ContactInformation.newBuilder()
                                .setValue("+33123456789")
                                .build())
                        .build())
                .build();

        final EventData dataOut = deserializationSchema.deserialize(dataIn.toByteArray());

        assertThat(dataOut).isNotNull();
    }
}