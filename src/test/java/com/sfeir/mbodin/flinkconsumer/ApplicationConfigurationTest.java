package com.sfeir.mbodin.flinkconsumer;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ApplicationConfigurationTest {

    private static final String[] MINIMAL_ARGUMENTS = new String[]{"--bootstrap-servers", "localhost:29092", "--topic-name", "private_mbodin_producer"};

    @Test
    void getBootstrapServers() {
        ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(MINIMAL_ARGUMENTS);

        assertThat(applicationConfiguration.getBootstrapServers()).isEqualTo("localhost:29092");
    }

    @Test
    void getGroupId() {
        ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration(MINIMAL_ARGUMENTS);

        assertThat(applicationConfiguration.getGroupId()).isEqualTo("flink-test-consumers");
    }

    @Test
    void test_validation() {
        assertThatThrownBy(ApplicationConfiguration::new)
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("bootstrap-servers")
        ;

        assertThatThrownBy(() -> new ApplicationConfiguration("--bootstrap-servers", "localhost:29092"))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("topic-name")
        ;
    }

    @Test
    void getTopicPartitions() {
        assertThat(new ApplicationConfiguration(MINIMAL_ARGUMENTS).getTopicPartitions())
                .hasSize(2)
                .allMatch(tp -> tp.topic().startsWith("private_mbodin_producer"));
    }
}