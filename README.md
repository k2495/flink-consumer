# flink-consumer

The purpose of this application is to put hands on how to work with [Flink](https://flink.apache.org/).

This application is part of a series and cover the event production operations.

The context of this application is the following:

* Consider it rationalize user contact operations produced by multiple sources.
* Those operations are categorized by one of the following verbs `CREATE`, `UPDATE` or `DELETE`.
* It aims to adapt the data stream to different outputs for further operations

## Protobuf classes generation

```shell
protoc --java_out=./src/main/java ./src/main/proto/producer.proto
```

## About rationalizing contact operations

Let's be frank, it is more about using `Flink` than implementing a real life concrete scenario... 
The idea is to proceed a reduction over events in a window of time, thus producing a merge event in the end.

For the window, the events are keyed by their card's nickname. A window end after a period of inactivity.

To resume, operations on cards of a given user are reduced to avoid redundancy over time.

## About dedicated outputs

Again, here the idea wasn't to be realistic... The idea is to distribute events from the data stream into dedicated 
Kafka topics to a given operation kind (`CREATE`, `UPDATE` and `DELETE`).

## Package as a container image

As [Flink documentation](https://nightlies.apache.org/flink/flink-docs-release-1.14/docs/deployment/resource-providers/standalone/docker/#application-mode)
stated, the `package` phase include the build of the image in Application mode. Thus, it can be used as long as the `command`
is overridden like the following: 
`standalone-job --job-classname com.sfeir.mbodin.flinkconsumer.FlinkConsumerApplication --bootstrap-servers kafka:9092 --topic-name private_multisources_producer`

Adapt the job arguments of course.

⚠️ The Maven plugin used is for `podman` only !

## Configuration

Check the file `application.properties` to get a complete overview of the configuration. Most have default value,
nevertheless some are mandatory:

* `application.bootstrap-servers` which represent where Kafka is reachable, expressed as `host:port`.
* `topic.name` which represent the name of the topic events will be produced to.

To fit Flink [Session Mode](https://nightlies.apache.org/flink/flink-docs-release-1.14/docs/deployment/overview/#session-mode),
most of the configuration can be overridden through program arguments.

⚠️ In Session mode, `application.properties` seems to be not loadable, **DO NOT FORGET** to pass program arguments when
submitting a new Job.
